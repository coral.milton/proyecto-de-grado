# FrontProyectoGrado

## Folder Structure

- **/assets:** Contains static resources such as images, fonts, audio files, etc.

- **/components:** Reusable components throughout the application.

- **/screens:** Screens of the application organized by functionality.

- **/navigation:** Navigation configuration and components.

- **/utils:** Generic functions and utilities used throughout the application.

- **/hooks:** Custom hooks for sharing logic between components.

- **/src:** Application-specific configuration files, such as environment settings, API keys, etc.

- **/styles:** Contains global and screen-specific styles.
  - `colors.js`: Definition of colors used in the application.
  - `fonts.js`: Definition of fonts used in the application.
  - `globalStyles.js`: Global styles such as text styles, generic buttons, etc.
  - **/screenStyles/**
    - `HomeScreenStyles.js`: Styles specific to the home screen.
    - `ProfileScreenStyles.js`: Styles specific to the profile screen.

- **/amplify:** Files and configurations related to AWS Amplify.
  - **/auth:** Authentication configuration, such as Amazon Cognito.
  - **/api:** API configuration, e.g., API Gateway, Lambda functions, etc.
  - **/storage:** Storage configuration, such as Amazon S3.
  - **/analytics:** Analytics or tracking configuration, e.g., Amplify Analytics.
  - **/function:** Configuration related to Lambda functions (if applicable).
  - **/graphql:** GraphQL API configuration (if using AppSync).

- **/docs:** Project documentation, such as style guides, usage guides, etc.
