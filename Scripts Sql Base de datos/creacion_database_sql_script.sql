
CREATE SEQUENCE prueba.forma_pago_id_pago_seq_1;

CREATE TABLE prueba.FORMA_PAGO (
                ID_PAGO INTEGER NOT NULL DEFAULT nextval('prueba.forma_pago_id_pago_seq_1'),
                FORMA_PAGO VARCHAR NOT NULL,
                CODIGO_PAGO VARCHAR NOT NULL,
                CONSTRAINT forma_pago_pk PRIMARY KEY (ID_PAGO)
);


ALTER SEQUENCE prueba.forma_pago_id_pago_seq_1 OWNED BY prueba.FORMA_PAGO.ID_PAGO;

CREATE SEQUENCE prueba.datos_clientes_id_cliente_seq;

CREATE TABLE prueba.DATOS_CLIENTES (
                ID_CLIENTE INTEGER NOT NULL DEFAULT nextval('prueba.datos_clientes_id_cliente_seq'),
                ID_IDENTIFICACION INTEGER NOT NULL,
                IDENTIFICACION VARCHAR NOT NULL,
                NOMBRE VARCHAR NOT NULL,
                APELLIDO VARCHAR NOT NULL,
                TELEFONO VARCHAR NOT NULL,
                DIRECCION VARCHAR NOT NULL,
                MAIL VARCHAR NOT NULL,
                CONSTRAINT datos_clientes_pk PRIMARY KEY (ID_CLIENTE)
);


ALTER SEQUENCE prueba.datos_clientes_id_cliente_seq OWNED BY prueba.DATOS_CLIENTES.ID_CLIENTE;

CREATE SEQUENCE prueba.factura_id_factura_seq;

CREATE TABLE prueba.FACTURA (
                ID_FACTURA INTEGER NOT NULL DEFAULT nextval('prueba.factura_id_factura_seq'),
                ID_CLIENTE INTEGER NOT NULL,
                ID_PAGO INTEGER NOT NULL,
                FECHA DATE NOT NULL,
                SUBTOTAL_12 REAL NOT NULL,
                SUB_TOTAL_0 REAL NOT NULL,
                SUBTOTAL_NO_OBJETO_IVA REAL NOT NULL,
                SUBTOTAL_EXENTO_IVA REAL NOT NULL,
                SUBTOTAL_SIN_IMPUESTOS REAL NOT NULL,
                DESCUENTO REAL NOT NULL,
                ICE REAL NOT NULL,
                IVA REAL NOT NULL,
                IRBPNR REAL NOT NULL,
                PROPINA REAL NOT NULL,
                TOTAL_FACTURA REAL NOT NULL,
                CONSTRAINT factura_pk PRIMARY KEY (ID_FACTURA)
);


ALTER SEQUENCE prueba.factura_id_factura_seq OWNED BY prueba.FACTURA.ID_FACTURA;

CREATE SEQUENCE prueba.detalle_factura_id_detalle_seq;

CREATE TABLE prueba.DETALLE_FACTURA (
                ID_DETALLE INTEGER NOT NULL DEFAULT nextval('prueba.detalle_factura_id_detalle_seq'),
                ID_FACTURA INTEGER NOT NULL,
                CODIGO_PRINCIPAL VARCHAR NOT NULL,
                CANTIDAD INTEGER NOT NULL,
                DESCRIPCION VARCHAR NOT NULL,
                DETALLE_ADICIONAL VARCHAR,
                PRECIO_UNITARIO REAL NOT NULL,
                SUBSIDIO REAL NOT NULL,
                PRECIO_SIN_SUBSIDIO REAL NOT NULL,
                DESCUENTO REAL NOT NULL,
                TOTAL REAL NOT NULL,
                CONSTRAINT detalle_factura_pk PRIMARY KEY (ID_DETALLE)
);


ALTER SEQUENCE prueba.detalle_factura_id_detalle_seq OWNED BY prueba.DETALLE_FACTURA.ID_DETALLE;

CREATE SEQUENCE prueba.tipo_id_tipo_seq_1;

CREATE TABLE prueba.TIPO (
                ID_TIPO INTEGER NOT NULL DEFAULT nextval('prueba.tipo_id_tipo_seq_1'),
                TIPO VARCHAR NOT NULL,
                CONSTRAINT tipo_pk PRIMARY KEY (ID_TIPO)
);


ALTER SEQUENCE prueba.tipo_id_tipo_seq_1 OWNED BY prueba.TIPO.ID_TIPO;

CREATE SEQUENCE prueba.inventario_productos_id_categoria_seq_1;

CREATE TABLE prueba.CATEGORIA (
                ID_CATEGORIA INTEGER NOT NULL DEFAULT nextval('prueba.inventario_productos_id_categoria_seq_1'),
                NOMBRE_CATEGORIA VARCHAR NOT NULL,
                CONSTRAINT categoria_pk PRIMARY KEY (ID_CATEGORIA)
);


ALTER SEQUENCE prueba.inventario_productos_id_categoria_seq_1 OWNED BY prueba.CATEGORIA.ID_CATEGORIA;

CREATE SEQUENCE prueba.datos_proveedor_id_proveedor_seq;

CREATE TABLE prueba.DATOS_PROVEEDOR (
                ID_PROVEEDOR INTEGER NOT NULL DEFAULT nextval('prueba.datos_proveedor_id_proveedor_seq'),
                IDENTIFICACION VARCHAR NOT NULL,
                NOMBRE VARCHAR NOT NULL,
                APELLIDO VARCHAR NOT NULL,
                NOMBRE_COMERCIAL VARCHAR NOT NULL,
                TELEFONO VARCHAR NOT NULL,
                DIRECCION VARCHAR NOT NULL,
                MAIL VARCHAR NOT NULL,
                CONSTRAINT datos_proveedor_pk PRIMARY KEY (ID_PROVEEDOR)
);


ALTER SEQUENCE prueba.datos_proveedor_id_proveedor_seq OWNED BY prueba.DATOS_PROVEEDOR.ID_PROVEEDOR;

CREATE SEQUENCE prueba.inventario_precios_codigo_seq_1;

CREATE TABLE prueba.INVENTARIO_PRODUCTOS (
                ID_PRODUCTO INTEGER NOT NULL DEFAULT nextval('prueba.inventario_precios_codigo_seq_1'),
                ID_CATEGORIA INTEGER NOT NULL,
                ID_TIPO INTEGER NOT NULL,
                NOMBRE_PRODUCTO VARCHAR NOT NULL,
                DETALLES VARCHAR,
                PREDICCION VARCHAR,
                CONSTRAINT inventario_productos_pk PRIMARY KEY (ID_PRODUCTO)
);


ALTER SEQUENCE prueba.inventario_precios_codigo_seq_1 OWNED BY prueba.INVENTARIO_PRODUCTOS.ID_PRODUCTO;

CREATE SEQUENCE prueba.kardex_id_registro_seq;

CREATE TABLE prueba.KARDEX (
                ID_REGISTRO INTEGER NOT NULL DEFAULT nextval('prueba.kardex_id_registro_seq'),
                ID_PRODUCTO INTEGER NOT NULL,
                FECHA DATE NOT NULL,
                DETALLE VARCHAR,
                TIPO_MOVIMIENTO VARCHAR NOT NULL,
                CANTIDAD INTEGER NOT NULL,
                VALOR_TOTAL REAL NOT NULL,
                T_CANTIDAD INTEGER NOT NULL,
                T_TOTAL REAL NOT NULL,
                CONSTRAINT kardex_pk PRIMARY KEY (ID_REGISTRO)
);


ALTER SEQUENCE prueba.kardex_id_registro_seq OWNED BY prueba.KARDEX.ID_REGISTRO;

CREATE SEQUENCE prueba.inventario_precios_codigo_seq_1_1;

CREATE TABLE prueba.INVENTARIO_PRECIOS (
                ID_PRODUCTO INTEGER NOT NULL DEFAULT nextval('prueba.inventario_precios_codigo_seq_1_1'),
                PRECIO_COMPRA REAL NOT NULL,
                PRECIO_VENTA_REFERENCIAL REAL NOT NULL,
                UNIDAD INTEGER NOT NULL,
                STOCK INTEGER NOT NULL,
                CONSTRAINT inventario_precios_pk PRIMARY KEY (ID_PRODUCTO)
);


ALTER SEQUENCE prueba.inventario_precios_codigo_seq_1_1 OWNED BY prueba.INVENTARIO_PRECIOS.ID_PRODUCTO;

CREATE SEQUENCE prueba.inventario_precios_codigo_seq;

CREATE TABLE prueba.CODIGO (
                ID_PROVEEDOR INTEGER NOT NULL,
                ID_PRODUCTO INTEGER NOT NULL DEFAULT nextval('prueba.inventario_precios_codigo_seq'),
                CODIGO_PRINCIPAL VARCHAR NOT NULL,
                CODIGO_PROVEDOR VARCHAR NOT NULL,
                CONSTRAINT codigo_pk PRIMARY KEY (ID_PROVEEDOR, ID_PRODUCTO)
);


ALTER SEQUENCE prueba.inventario_precios_codigo_seq OWNED BY prueba.CODIGO.ID_PRODUCTO;

ALTER TABLE prueba.FACTURA ADD CONSTRAINT forma_pago_factura_fk
FOREIGN KEY (ID_PAGO)
REFERENCES prueba.FORMA_PAGO (ID_PAGO)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE prueba.FACTURA ADD CONSTRAINT datos_clientes_factura_fk
FOREIGN KEY (ID_CLIENTE)
REFERENCES prueba.DATOS_CLIENTES (ID_CLIENTE)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE prueba.DETALLE_FACTURA ADD CONSTRAINT factura_detalle_factura_fk
FOREIGN KEY (ID_FACTURA)
REFERENCES prueba.FACTURA (ID_FACTURA)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE prueba.INVENTARIO_PRODUCTOS ADD CONSTRAINT tipo_inventario_productos_fk
FOREIGN KEY (ID_TIPO)
REFERENCES prueba.TIPO (ID_TIPO)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE prueba.INVENTARIO_PRODUCTOS ADD CONSTRAINT categoria_inventario_productos_fk
FOREIGN KEY (ID_CATEGORIA)
REFERENCES prueba.CATEGORIA (ID_CATEGORIA)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE prueba.CODIGO ADD CONSTRAINT datos_distribuidor_distribuidor_fk
FOREIGN KEY (ID_PROVEEDOR)
REFERENCES prueba.DATOS_PROVEEDOR (ID_PROVEEDOR)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE prueba.INVENTARIO_PRECIOS ADD CONSTRAINT inventario_productos_inventario_precios_fk
FOREIGN KEY (ID_PRODUCTO)
REFERENCES prueba.INVENTARIO_PRODUCTOS (ID_PRODUCTO)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE prueba.KARDEX ADD CONSTRAINT inventario_productos_kardex_fk
FOREIGN KEY (ID_PRODUCTO)
REFERENCES prueba.INVENTARIO_PRODUCTOS (ID_PRODUCTO)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE prueba.CODIGO ADD CONSTRAINT inventario_productos_codigo_fk
FOREIGN KEY (ID_PRODUCTO)
REFERENCES prueba.INVENTARIO_PRODUCTOS (ID_PRODUCTO)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;